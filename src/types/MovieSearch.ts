import { API_URL, API_KEY_v3, IMG_URL } from '../utils';
import { Movie, SearchType } from '../types/Interfaces';

const randomMovie = document.getElementById('random-movie') as HTMLElement;
const filmsContainter = document.getElementById(
    'film-container'
) as HTMLElement;

export function getMovies(url: string): void {
    fetch(
        `${API_URL}/search/movie?api_key=${API_KEY_v3}&language=en-US&query=${url}&page=1&include_adult=false`
    )
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            showMovies(data.results);
            showRandomMovie(data.results);
        });
}

export function getTypeMovies(searchType: SearchType = 'popular'): void {
    fetch(
        `${API_URL}/movie/${searchType}?api_key=${API_KEY_v3}&language=en-US&page=1`
    )
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            showMovies(data.results);
            showRandomMovie(data.results);
        });
}

export function getMovieDetails(movie_id: number): void {
    fetch(`${API_URL}/movie/${movie_id}?api_key=${API_KEY_v3}>&language=en-US`)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            console.log(data);
        });
}

function showRandomMovie(data: Movie[]): void {
    randomMovie.innerHTML = '';

    const randomitem = data[Math.floor(Math.random() * data.length)];

    const { title, poster_path, overview } = randomitem;

    const randomItemEl = document.createElement('div');
    randomItemEl.classList.add('row');
    randomItemEl.classList.add('py-lg-5');

    randomItemEl.style.backgroundImage = `url(${IMG_URL + poster_path})`;
    randomItemEl.style.backgroundRepeat = 'no-repeat';
    randomItemEl.style.backgroundPosition = 'center';
    randomItemEl.style.backgroundSize = 'cover';

    randomItemEl.innerHTML = `
        <div
                        class="col-lg-6 col-md-8 mx-auto"
                        style="background-color: #2525254f"
                    >
                        <h1 id="random-movie-name" class="fw-light text-light">
                            ${title}
                        </h1>
                        <p
                            id="random-movie-description"
                            class="lead text-white"
                        >
                            ${overview}
                        </p>
                    </div>
    `;

    randomMovie.appendChild(randomItemEl);
}

function showMovies(data: Movie[]): void {
    filmsContainter.innerHTML = '';

    data.forEach((movie: Movie) => {
        const { title, poster_path, overview, release_date } = movie;
        const movieEl = document.createElement('div');
        movieEl.classList.add('col-lg-3');
        movieEl.classList.add('col-md-4');
        movieEl.classList.add('col-12');
        movieEl.classList.add('p-2');
        movieEl.innerHTML = `<div class="card shadow-sm">
        <img
            src="${
                poster_path
                    ? IMG_URL + poster_path
                    : 'http://via.placeholder.com/1080x1580'
            }" alt="${title}"
        />
        <svg
            xmlns="http://www.w3.org/2000/svg"
            stroke="red"
            fill="#ff000078"
            width="50"
            height="50"
            class="
                bi bi-heart-fill
                position-absolute
                p-2
            "
            viewBox="0 -2 18 22"
        >
            <path
                fill-rule="evenodd"
                d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
            />
        </svg>
        <div class="card-body">
            <p class="card-text truncate">
                ${overview}
            </p>
            <div
                class="
                    d-flex
                    justify-content-between
                    align-items-center
                "
            >
                <small class="text-muted"
                    >${release_date}</small
                >
            </div>
        </div>
    </div>
    `;
        filmsContainter.appendChild(movieEl);
    });
}
