export interface IStorageItem {
    key: string;
    value: string;
}

export class StorageItem {
    key: string;
    value: string;

    constructor(data: IStorageItem) {
        this.key = data.key;
        this.value = data.value;
    }
}

export class LocalStorageWorker {
    islocalStorage: boolean;

    constructor() {
        this.islocalStorage =
            typeof window['localStorage'] != 'undefined' &&
            window['localStorage'] != null;
    }

    add(key: string, item: string): void {
        if (this.islocalStorage) {
            localStorage.setItem(key, item);
        }
    }

    getAllItems(): Array<StorageItem> {
        const list = new Array<StorageItem>();

        for (let i = 0; i < localStorage.length; i++) {
            const key = localStorage.key(i);
            const value = localStorage.getItem(key as string);

            list.push(
                new StorageItem({
                    key: key as string,
                    value: value as string,
                })
            );
        }

        return list;
    }

    getAllValues(): Array<any> {
        const list = new Array<any>();

        for (let i = 0; i < localStorage.length; i++) {
            const key = localStorage.key(i);
            const value = localStorage.getItem(key as string);

            list.push(value);
        }

        return list;
    }

    get(key: string): string | null {
        if (this.islocalStorage) {
            const item = localStorage.getItem(key);
            return item;
        } else {
            return null;
        }
    }

    remove(key: string): void {
        if (this.islocalStorage) {
            localStorage.removeItem(key);
        }
    }

    clear(): void {
        if (this.islocalStorage) {
            localStorage.clear();
        }
    }
}

export class IdStorage {
    storageWorker: LocalStorageWorker;

    storageKey: string;

    ids: Array<string>;

    constructor(storageKey: string) {
        this.storageWorker = new LocalStorageWorker();
        this.storageKey = storageKey;
        this.ids = new Array<string>();
        this.activate();
    }

    activate(): void {
        this.loadAll();
    }

    loadAll(): void {
        const storageData = this.storageWorker.get(this.storageKey);

        if (storageData != null && storageData.length > 0) {
            const ids = JSON.parse(storageData);
            console.log(ids);
            if (ids != null) {
                this.ids = ids;
            }
            console.log(this.ids);
        }
    }

    addId(id: string): void {
        if (id.length > 0) {
            console.log(this.ids);

            this.save();
        }
    }

    clear(): void {
        this.storageWorker.add(this.storageKey, '');
    }

    save(): void {
        const jsonIds = JSON.stringify(this.ids);
        this.storageWorker.add(this.storageKey, jsonIds);
    }
}
