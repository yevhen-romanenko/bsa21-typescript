export enum TypeofSearch {
    popular,
    upcoming,
    top_rated,
}

interface IMovie {
    movieId: number;
    imgSrc: string;
    overview: string;
    dateTime: string;
}

export type SearchType = 'popular' | 'upcoming' | 'toprated' ;

export type Movie = {
    id?: number;
    overview?: string;
    title?: string;
    release_date?: string;
    poster_path?: string;
};

type MapperFunc<T, K> = (element: T) => K;

const customMap = <T, K>(mapper: MapperFunc<T, K>, values: T[]) => {
    const list = [];
    for (const el of values) {
        const result = mapper(el);
        list.push(result);
    }
    return list;
};

const arrayOfFilms = customMap<number, string>(
    (item) => `${item}`,
    [1, 2, 3, 4, 5]
);

console.log(arrayOfFilms);
