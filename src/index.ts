import { SearchType } from './types/Interfaces';
import { getTypeMovies, getMovies } from './types/MovieSearch';
import {} from "./types/StorageHelper";

export async function render(): Promise<void> {
    const search = document.getElementById('search') as HTMLElement;
    const form = document.getElementById('form') as HTMLElement;
       
    const activeSearchTypeButton =
        document.querySelectorAll<HTMLElement>('.btn-check');

    getTypeMovies();

    activeSearchTypeButton.forEach((button) => {
        button.addEventListener('click', () => {
            const searchType: SearchType = button.id as SearchType;
            check(searchType);
            getTypeMovies(searchType);
        });
    });

    form.addEventListener('submit', (e: Event) => {
        e.preventDefault();
        getCheckedType();
        const searchString: string = (search as HTMLInputElement).value;
        if (searchString) {
            getMovies(searchString);
        } else {
            getTypeMovies();
        }
    });

    function check(searchType: SearchType) {
        const checkButton = document.getElementById(
            searchType
        ) as HTMLInputElement;
        checkButton.checked = true;
    }

    function getCheckedType() {
        const activeSearchTypeButton =
            document.querySelectorAll<HTMLElement>('.btn-check');
        activeSearchTypeButton.forEach((button) => {
            if ((button as HTMLInputElement).checked) {
                const checkedType: string = button.id;
                return checkedType;
            }
        });
    }
}
